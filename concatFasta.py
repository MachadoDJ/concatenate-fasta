#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# concatFasta.py
# Concatenates multiple alignments in FASTA format

# Import modules and libraries
import argparse
from Bio import SeqIO

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="List of FASTA files (separated by space)",type=str, nargs='+', required=True)
args=parser.parse_args()

# Define functions
def get_all_terminal_names(): # Get the names of the terminals in every alignment file
	all_terminal_names = [] # Creates am empty list variable to hold the terminal names
	for file in args.input: # For every file in the input
		for record in SeqIO.parse(file, "fasta"): # For every sequence in that file
			if not record.id in all_terminal_names: # Check if that terminal is already on the list
				all_terminal_names += [record.id] # Append a new terminal to the list
	return all_terminal_names # Return list with all terminals

def get_all_alignments_lengths(): # Gets the alignment length of every alignment
	all_alignments_lengths = {} # Creates an empty dictionary
	for file in args.input: # For every file in the input
		for record in SeqIO.parse(file, "fasta"): # For every sequence in the file
			all_alignments_lengths[file] = len(record.seq) # Associate the file with the alignment length
			break # I can break here because I only need to read the first sequence in each file, since they are all the same length
	return all_alignments_lengths # Return dictionary with the name of the file and its alignment length

def concatenate_the_alignment(all_terminal_names, all_alignments_lengths):
	concatenated_sequences = {} # Empty dictionary to hold the results
	for file in all_alignments_lengths: # Iterate over every entry in the dictionary
		used = [] # Keep track of the sequences that have been delt with
		for record in SeqIO.parse(file, "fasta"): # Use BioPython to head the sequences
			used += [record.id] # Append id so you know you already used it for the current file
			if not record.id in concatenated_sequences: # For new sequences
				concatenated_sequences[record.id] = record.seq # Add new
			else: # For things that are already there
				concatenated_sequences[record.id] += record.seq # Append
		for id in all_terminal_names: # For loop before closing the file, to add missing sequeces
			if not id in used: # Only if you have not used it in this file
				if not id in concatenated_sequences: # For new sequences
					concatenated_sequences[id] = "{}".format("?" * all_alignments_lengths[file]) # Add new
				else: # Or
					concatenated_sequences[id] += "{}".format("?" * all_alignments_lengths[file]) # Append
	return concatenated_sequences # Return concatenated sequences

def report_results(concatenated_sequences): # This function will print the concatenated sequence into your screen
	for id in concatenated_sequences: # For each entry on this dictionary
		print(">{}\n{}\n".format(id, concatenated_sequences[id])) # Print the item and its meaning on the dictionary, using the FATSA format
	return # End function and returns nothing

# Execute functions
all_terminal_names = get_all_terminal_names() # Execute function to get all terminal names, in case some files have different terminals than the other
all_alignments_lengths = get_all_alignments_lengths() # Execute function to get the length of the alignment in each file
concatenated_sequences = concatenate_the_alignment(all_terminal_names, all_alignments_lengths) # Function to concatenate the alignments given all the terminal names and all the alignment lengths
report_results(concatenated_sequences) # Report results


# Quit
exit()

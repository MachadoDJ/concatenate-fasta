# Concatenating FASTA Alignments

## Contents

- FASTA alignments with overlapping but non-identical sets of terminals: `sample1.fasta` and `sample2.fasta`
- Script to remove trailing gaps (do this first for each indivirual alignment): `handleTrailingGaps.py`
- Script to concatenate alignments: `concatFasta.py`
- Combine different types of data into a large Nexus file: `concatData.py` (requires [dendropy]([https://dendropy.org](https://dendropy.org/)))
- Convert alignments from Phylip to FASTA: `phy2fas.py`

## Example pipeline

### Input files

- `sample1.fasta`
- `sample2.fasta`

### Remove trailing gaps

```bash
python3 handleTrailingsGaps.py -i sample1.fasta > sample2_mod.fasta
python3 handleTrailingsGaps.py -i sample2.fasta > sample2_mod.fasta
```

### Concatenate files

```bash
python3 concatFasta.py -i *_mod.fasta > concat.fasta
```

## Combine different types of data into a large Nexus file

### Dependencies

- [dendropy]([https://dendropy.org](https://dendropy.org/))

### Input files

- `sample1.fasta`
- `sample2.fasta`
- `sample3.nex`

### Remove trailing gaps

```bash
python3 handleTrailingsGaps.py -i sample1.fasta > sample2_mod.fasta
python3 handleTrailingsGaps.py -i sample2.fasta > sample2_mod.fasta
```

### Combine different types of files

```bash
concatData.py -d sample[1,2].fasta -m sample3.nex -v
```

## Convert alignments from Phylip to FASTA

```bash
python3 phy2fas.py -i sample4.phy
```


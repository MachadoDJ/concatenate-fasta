#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# combineMatricesForPhylogenetics.py
# Concatenates morphology data in nexus and molecular data in fasta.

# Import modules and libraries
import argparse, sys, re
from os import path
try:
	import dendropy
except:
	sys.stderr.write("! ERROR: Could not load dendropy (see https://dendropy.org).\n")
	exit()

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-d","--dna",help="DNA alignments in Fasta format (accepts lists)",nargs="+",type=str,required=False)
parser.add_argument("-i","--ignore",help="List of taxa to be removed",type=str,required=False)
parser.add_argument("-m","--morphology",help="Morphology matrix in Nexus format",type=str,required=False)
parser.add_argument("-p","--prefix",help="Prefix for output files (dafault = 'out'). Can replace existing files.",type=str,default="out",required=False)
parser.add_argument("-r","--rename",help="Produce copies of the output files with simplier taxon names",action="store_true",default=False,required=False)
parser.add_argument("-s","--symbol",help="Symbol for missing data (dafault = '?'). Does not replace existing symbols.",type=str,default="?",required=False)
parser.add_argument("-v","--verbose",help="Increases output verbosity",action="store_true",default=False,required=False)
parser.add_argument("--prevalence",help="Reports the prevalence of each terminal and quit",action="store_true",default=False,required=False)
args=parser.parse_args()

# Define functions
def check_input():
	"""Verify if there is any input data and which files were selected this way"""
	if(args.verbose):
		if(args.morphology):
			sys.stdout.write("> Morphology:\n* {}\n".format(args.morphology))
		if(args.dna):
			sys.stdout.write("> DNA alignments:\n* {}\n".format("\n* ".join(args.dna)))
	if (args.morphology or args.dna):
		return True
	else:
		return False

def main(ignore):
	"""This is the backbone function of this program"""
	partitions = {}
	count_partitions = 0
	all_taxa = dendropy.TaxonNamespace()
	if(args.verbose):
		sys.stdout.write("> Start processing data\n")
	if(args.morphology):
		morphology_data, all_taxa = get_data_from_morphology(all_taxa)
		count_partitions += 1
		partitions[count_partitions] = {"file" : args.morphology, "length" : morphology_data.max_sequence_size}
	if(args.dna):
		list_of_dna_matrices = []
		for fasta_file in args.dna:
			dna_data, all_taxa = get_data_from_dna(fasta_file, all_taxa)
			list_of_dna_matrices += [dna_data]
			count_partitions += 1
			partitions[count_partitions] = {"file" : fasta_file, "length" : dna_data.max_sequence_size}
		total_dna = ""
		for dna_data in list_of_dna_matrices:
			"""Add missing taxa and pad out all sequences to each sequence until all lengths are equal"""
			dna_data.fill_taxa()
			dna_data.fill(dendropy.datamodel.charstatemodel.StateIdentity(symbol=args.symbol, index=None, state_denomination=0, member_states=None), size=None, append=True)
			if(total_dna):
				total_dna = dendropy.DnaCharacterMatrix.concatenate([total_dna, dna_data])
			else:
				total_dna = dna_data
		for taxon in all_taxa:
			if(taxon.label in ignore):
				total_dna.remove_sequences([taxon])
		ntax = len(total_dna)
		nexus = re.sub("NTAX\s*=\s*\d+", "NTAX={}".format(ntax), total_dna.as_string(schema="nexus"))
		if(args.verbose):
			sys.stdout.write("> DNA data ({}):\n* {} taxa\n* {} characters\n".format(total_dna, len(total_dna), total_dna.max_sequence_size))
			sys.stdout.write("// Start Fasta DNA //\n{}// End Fasta DNA //\n".format(total_dna.as_string(schema="fasta")))
			sys.stdout.write("// Start Nexus DNA //\n{}// End Nexus DNA //\n".format(nexus))
		handle = open("{}_dna.fas".format(args.prefix), "w")
		handle.write(total_dna.as_string(schema="fasta"))
		handle.close()
		handle = open("{}_dna.nex".format(args.prefix), "w")
		handle.write(nexus)
		handle.close()
	if(args.verbose):
		sys.stdout.write("// Begin Partitions DNA //\n{}\n// End Partitions DNA //\n".format(report_dna_partitions(partitions)))
	if(args.morphology):
		morphology_data.fill_taxa()
		morphology_data.fill(dendropy.datamodel.charstatemodel.StateIdentity(symbol=args.symbol, index=None, state_denomination=0, member_states=None), size=None, append=True)
		for taxon in all_taxa:
			if(taxon.label in ignore):
				morphology_data.remove_sequences([taxon])
		ntax = len(morphology_data)
		nexus = re.sub("NTAX\s*=\s*\d+", "NTAX={}".format(ntax), morphology_data.as_string(schema="nexus"))
		if(args.verbose):
			sys.stdout.write("// Start Nexus Morphology //\n{}// End Nexus Morphology //\n".format(nexus))
		handle = open("{}_morphology.nex".format(args.prefix), "w")
		handle.write(nexus)
		handle.close()
	if(args.rename):
		simplify_terminal_names()
	if(args.morphology):
		correct_symbols("morphology")
	if(args.dna):
		correct_symbols("dna")
	if(args.morphology and args.dna):
		nexus_into_tnt()
	return

def nexus_into_tnt():
	file = "{}_morphology_renamed.nex".format(args.prefix)
	if(path.exists(file)):
		handle = open(file, "r")
		data = handle.read()
		handle.close()
		mor    = re.compile("\s+MATRIX\s+(.+?);",re.I|re.M|re.S).findall(data)[0].strip()
		ntax   = int(re.compile("NTAX\s*=\s*(\d+)", re.I|re.S|re.M).findall(data)[0])
		nchar1 = int(re.compile("NCHAR\s*=\s*(\d+)", re.I|re.S|re.M).findall(data)[0])
		handle = open("{}_dna_renamed.nex".format(args.prefix), "r")
		data = handle.read()
		handle.close()
		dna    = re.compile("\s+MATRIX\s+(.+?);",re.I|re.M|re.S).findall(data)[0].strip()
		nchar2 = int(re.compile("NCHAR\s*=\s*(\d+)", re.I|re.S|re.M).findall(data)[0])
		out    = "nstates 32;\nxread\n{} {}\n&[num]\n\t{}\n&[dna]\n\t{}\n;\nproc /;\n".format(nchar1 + nchar2, ntax, mor, dna)
		out    = re.sub("{", "[", out)
		out    = re.sub("}", "]", out)
		out    = re.sub(",", " ", out)
		handle = open("{}_combined_renamed.tnt".format(args.prefix), "w")
		handle.write(out)
		handle.close()
	else:
		pass
	return

def correct_symbols(label):
	"""Fix SYMBOLS and SETS"""
	if(label=="morphology"):
		handle = open(args.morphology, "r")
		symbols = re.compile("(symbols\s*=\s*[\"\'].+?[\"\'])", re.I|re.M|re.S).findall(handle.read())[0]
		handle.close()
	for file in ["{}_{}.nex".format(args.prefix, label), "{}_{}_renamed.nex".format(args.prefix, label)]:
		if(path.exists(file)):
			handle = open(file, "r")
			data = handle.read()
			handle.close()
			data = re.sub("BEGIN SETS;.+?END;", "", data, flags = re.IGNORECASE|re.MULTILINE) # Remove sets
			if(label=="morphology"):
				data = re.sub("symbols\s*=\s*[\"\'].+?[\"\']", symbols, data, flags = re.IGNORECASE|re.MULTILINE) # Fix symbols
			# Convert to simple format
			ntax  = re.compile("(NTAX\s*=\s*\d+)", re.I|re.S|re.M).findall(data)[0]
			nchar = re.compile("(NCHAR\s*=\s*\d+)", re.I|re.S|re.M).findall(data)[0]
			body  = re.compile("\s+(FORMAT\s+.+?END\s*;)", re.I|re.S|re.M).findall(data)[0]
			data  = "#NEXUS\n\nBEGIN DATA;\n\tDIMENSIONS {} {};\n\t{}".format(ntax, nchar, body)
			# Indentation
			data = re.sub("^ +", "\t", data, flags = re.IGNORECASE|re.MULTILINE|re.DOTALL)
			data = re.sub(r"^\t([^\s]+) +([^\s]+)\n", r"\t\1    \2\n", data, flags = re.IGNORECASE|re.MULTILINE|re.DOTALL)
			# Deal with parenthesis
			data = re.sub(r"\(([^\)]+)\)", r"{\1}", data, flags = re.IGNORECASE|re.MULTILINE|re.DOTALL)
			# Report
			handle = open(file, "w")
			handle.write(data)
			handle.close()
	return

def simplify_terminal_names():
	"""Produce copies of the output files with simplier taxon names"""
	if(args.morphology):
		handle = open("{}_morphology.nex".format(args.prefix), "r")
		matrix = handle.read()
		handle.close()
		all_taxa = [taxon for taxon in sorted(sorted(re.sub("\s+","\n",re.compile("TAXLABELS(.+?);\s*END\s*;",re.M|re.I|re.S).findall(matrix)[0].strip()).split("\n")), key = len)[::-1]]
		for i in range(0, len(all_taxa)):
			matrix = matrix.replace("{}".format(all_taxa[i]), "t{}".format(i+1))
		handle = open("{}_morphology_renamed.nex".format(args.prefix), "w")
		handle.write(matrix)
		handle.close()
	if(args.dna):
		handle = open("{}_dna.nex".format(args.prefix), "r")
		matrix = handle.read()
		handle.close()
		all_taxa = [taxon for taxon in sorted(sorted(re.sub("\s+","\n",re.compile("TAXLABELS(.+?);\s*END\s*;",re.M|re.I|re.S).findall(matrix)[0].strip()).split("\n")), key = len)[::-1]]
		for i in range(0, len(all_taxa)):
			matrix = matrix.replace("{}".format(all_taxa[i]), "t{}".format(i+1))
		handle = open("{}_dna_renamed.nex".format(args.prefix), "w")
		handle.write(matrix)
		handle.close()
		handle = open("{}_dna.fas".format(args.prefix), "r")
		matrix = handle.read()
		handle.close()
		for i in range(0, len(all_taxa)):
			matrix = matrix.replace("{}".format(all_taxa[i].strip("'")), "t{}".format(i+1))
		handle = open("{}_dna_renamed.fas".format(args.prefix), "w")
		handle.write(matrix)
		handle.close()
	synonyms = ""
	for i in range(0, len(all_taxa)):
		synonyms += "{}\t{}\n".format(all_taxa[i].strip("'"), "t{}".format(i+1))
	handle = open("{}_synonyms.tsv".format(args.prefix), "w")
	handle.write(synonyms)
	handle.close()
	return

def report_dna_partitions(partitions):
# 	del partitions[1]
	"""Prints DNA partitions in Nexus format"""
	output="#NEXUS\n\nBEGIN SETS;\n"
	y = 1
	for position in sorted(partitions):
		x = partitions[position]["length"]
		output+="    charset {0} = {1}-{2};\n".format(partitions[position]["file"], y, y+x-1)
		y = y + x
	output+="END;\n"
	handle = open("{}_partitions.nex".format(args.prefix), "w")
	handle.write(output)
	handle.close()
	return output

def get_data_from_dna(fasta_file, all_taxa):
	"""Extracts the terminal names and other information from the provided alignment in Fasta format"""
	try:
		dna_data = dendropy.DnaCharacterMatrix.get(path=fasta_file, schema="fasta", taxon_namespace=all_taxa)
	except:
		sys.stderr.write("!!!Something went wrong while reading your alignment data ({0})!!!\n [What should I do?] Please check file {0} for posible errors or formating problems. Make sure data is aligned DNA in Fasta format and keep sequence names as simple as possible.\n".format(fasta_file))
		exit()
	else:
		if(args.verbose):
			sys.stdout.write("> DNA data ({}):\n* {} taxa\n* {} characters\n".format(fasta_file, len(dna_data), dna_data.max_sequence_size))
	return dna_data, all_taxa

def get_data_from_morphology(all_taxa):
	"""Extracts the terminal names and other information from the provided data in Nexus format"""
	try:
		morphology_data = dendropy.StandardCharacterMatrix.get(path=args.morphology, schema="nexus", taxon_namespace=all_taxa)
	except:
		sys.stderr.write("!!!Something went wrong while reading your morphological data ({0})!!!\n [What should I do?] Please check file {0} for posible errors or formating problems. You may want to export the Nexus file from Mesquite as a simplified Nexus file using DATA instead of TAXA/CHARACTERS blocks.\n".format(args.morphology))
		exit()
	else:
		if(args.verbose):
			sys.stdout.write("> Morphological data:\n* {} taxa\n* {} characters\n".format(len(morphology_data), morphology_data.max_sequence_size))
	return morphology_data, all_taxa

def read_list_of_taxa_to_ignore():
	"""This will get the list of taxa to be discarded, if any"""
	handle = open(args.ignore, "r")
	ignore = sorted(re.sub("\s+", "\n", handle.read()).strip().split("\n"))
	handle.close()
	return ignore

def report_prevalence():
	"""Report the prevalence of each taxon"""
	prevalence = {}
	if(args.morphology):
		try:
			taxa = dendropy.TaxonNamespace()
			morphology_data = dendropy.StandardCharacterMatrix.get(path=args.morphology, schema="nexus", taxon_namespace=taxa)
		except:
			sys.stderr.write("!!!Something went wrong while reading your morphological data ({0})!!!\n [What should I do?] Please check file {0} for posible errors or formating problems. You may want to export the Nexus file from Mesquite as a simplified Nexus file using DATA instead of TAXA/CHARACTERS blocks.\n".format(args.morphology))
			exit()
		else:
			for taxon in taxa:
				taxa = dendropy.TaxonNamespace()
				prevalence[taxon.label] = ["{}".format(args.morphology)]
	if(args.dna):
		for fasta_file in args.dna:
			try:
				taxa = dendropy.TaxonNamespace()
				dna_data = dendropy.DnaCharacterMatrix.get(path=fasta_file, schema="fasta", taxon_namespace=taxa)
			except:
				sys.stderr.write("!!!Something went wrong while reading your alignment data ({0})!!!\n [What should I do?] Please check file {0} for posible errors or formating problems. Make sure data is aligned DNA in Fasta format and keep sequence names as simple as possible.\n".format(fasta_file))
				exit()
			else:
				for taxon in taxa:
					taxa = dendropy.TaxonNamespace()
					if(taxon.label in prevalence):
						prevalence[taxon.label] += ["{}".format(fasta_file)]
					else:
						prevalence[taxon.label] = ["{}".format(fasta_file)]
	all_taxa = [t for t in sorted(prevalence)]
	sets = sorted(list(set([s for t in sorted(prevalence) for s in prevalence[t]])))
	output = "\n\t{}\n".format("\t".join(sets))
	for taxon in all_taxa:
		output += "{}".format(taxon)
		for partition in sets:
			if(partition in prevalence[taxon]):
				output += "\t1"
			else:
				output += "\t0"
		output += "\n"
	handle = open("{}_prevalence.tsv".format(args.prefix), "w")
	handle.write(output)
	handle.close()
	return

# Execute functions

if(args.prevalence):
	report_prevalence()
	exit()

if(check_input()):
	if(args.ignore):
		ignore = read_list_of_taxa_to_ignore()
	else:
		ignore = []
	main(ignore)

# Quit
exit()

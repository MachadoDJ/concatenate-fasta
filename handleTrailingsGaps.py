#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# handleTrailingsGaps.py
# Removes triling gaps or replace them by Ns

# By Denis Jacob Machado on April 19, 2018
# Note: This algorithm is case sensitive
# How to run the example: python3 handleTrailingsGaps.py -i example_with_trailing_gaps.fasta > output.fasta

# Import modules and libraries
import argparse, re

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="Input file (alignment in FASTA format containing trailing gaps)",type=str,required=True)
parser.add_argument("-g","--gap",help="Gap symbol (default = '-')",type=str,default="-",required=False)
parser.add_argument("-m","--missing",help="Missing symbol (default = 'N')",type=str,default="N",required=False)
parser.add_argument("-d","--delete",help="Delete trailing gaps instead of replacing them",action="store_true",default=False,required=False)
args=parser.parse_args()

# Define functions
def read_fasta(): # Reads the input file and parses its contents into a dictionary
	alignment = {} # Defines an empty dictionary called alignment. Dictionaries look like: dic = {word1 : description1, workd2 : description2 ...}.
	handle = open(args.input,"r") # Open the input in read mode
	for header, sequence in re.compile("(>[^\n\r]+)([^>]+)", re.M | re.S).findall(handle.read()):
		header = header.strip() # Removes invisible characters from each end of header
		sequence = re.sub("\s", "", sequence) # Removes invisible characters the sequence
		alignment[header] = sequence # Add the entry header to the alignment dictionary and associate it with the respective sequence
	handle.close() # Close the input file
	return alignment # Finish function and return the alignment dictionary

def handle_trailing_gaps(alignment): # Edit the sequences in the alignment
	for header in alignment: # For each term in the dictionary
		sequence = alignment[header] # Get the sequence
		gap = args.gap # Defines the gap symbol
		if(args.delete): # Defines the symbol for missing data
			mis = ""
		else:
			mis = args.missing
		try: # Tries to find gaps in the beginning of the sequence
			left = len(re.compile("^{}+".format(gap)).findall(sequence)[0])
		except: # Exception if there are no gaps in the beginning of the sequence
			left = 0
		try: # Tries to find gaps in the end of the sequence
			right = len(re.compile("{}+$".format(gap)).findall(sequence)[0])
		except: # Exception if there are no gaps in the end of the sequence
			right = 0
		sequence = sequence[left:len(sequence)-right] # Takes the part of the sequence between the trailing gaps
		sequence = "{}{}{}".format(left*mis,sequence,right*mis) # Edits the sequences, replacing the trailing gaps
		alignment[header] = sequence # Updates the dictionary
	return alignment # Returns the edited dictionary

def report(alignment): # This function prints the results into the STDOUT
	for head in alignment: # For each item in the dictionary
		print("{}\n{}".format(head,alignment[head])) # Print the corresponding header and sequence into the STDOUT
	return

# Execute functions
alignment = read_fasta() # Execute function read_fasta, which will return variable alignment
handle_trailing_gaps(alignment) # Execute function handle_trailing_gaps, giving the alignment variable to it
report(alignment) # Execute function report, giving the alignment variable to it

# Quit
exit()

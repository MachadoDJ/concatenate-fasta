#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# phy2fas
# Converts molecular alignments in Phylip format into FASTA format.

##
# Import libraries
##

import argparse, re, sys
from os import path

##
# Parse arguments from the command line
##

parser=argparse.ArgumentParser()
parser.add_argument("-i", "--input", help = "One or more molecular alignments in Phylip format", nargs = '+', type = str, required = True)
parser.add_argument("-s", "--suffix", help = "Suffix for output files (default = 'fasta')", default = "fasta", type = str, required = False)
args = parser.parse_args()

##
# Define functions
##

def main(infile, outfile):
	handle = open(infile, 'r')
	data = handle.read().strip()
	data = re.sub(r"[\r\n]", "\n", data).split("\n")
	handle.close()
	handle = open(outfile, 'w')
	terminals, length = [int(i) for i in re.compile("(\d+)[\s ]+(\d+)").findall(data[0])[0]]
	for line in data[1:]:
		count = 0
		seq = []
		name = []
		for char in line[::-1]:
			if(count<length):
				if(char==" " or char =="\t"):
					pass
				else:
					count+=1
					seq+=[char]
			else:
				name+=[char]
		handle.write(">{}\n{}\n".format(''.join(name[::-1]), ''.join(seq[::-1])))
	handle.close()
	return

##
# Execute functions
##

for infile in args.input:
	if(path.exists(infile)):
		outfile = "{}.{}".format(infile, args.suffix)
		main(infile, outfile) # This is the main fuction
	else:
		sys.stderr.write("! Error: cannot find file {}.\n".format(file))
		exit()
exit() # Quit this script
